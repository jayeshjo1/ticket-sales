package assignment6;

/**
 * TicketDriver class will simply contain the main method that will run the entire show.
 * <br>
 * <br>
 * It will first create an instance of {@link Theater} and create three instances of {@link BoxOffice}, BoxOffice A, BoxOffice B,
 * and Box Office C. For each instance of BoxOffice, it will make a thread and start running each thread, further specified in 
 * {@link BoxOffice#run()}.
 * @author Jayesh
 *
 */
public class TicketDriver{
    public static void main (String args[])
    {
	Theater bates = new Theater();
	
	BoxOffice officeA = new BoxOffice('A', bates);
	BoxOffice officeB = new BoxOffice('B', bates);
	BoxOffice officeC = new BoxOffice('C', bates);
	
	Thread t1 = new Thread(officeA);
	Thread t2 = new Thread(officeB);
	Thread t3 = new Thread(officeC);
	
	t1.start();
	t2.start();
	t3.start();
    }
}
