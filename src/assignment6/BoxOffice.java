package assignment6;

import java.util.Random;

/**
 * Runnable class which can be run as threads. When created, it requires the user to provide a character
 * that represents the letter name of the Box Office, and the Theater to which the Box Office will sell 
 * tickets from. The run method will first create a random number of clients from 100 - 1000 and find the
 * best seat available for them, mark it available, and print out that they have claimed that Seat. 
 * 
 * @author Jayesh
 *
 */
public class BoxOffice implements Runnable{
    private char id;
    private Theater theater;
    BoxOffice(char c, Theater t)
    {
	id = c;
	theater = t;
    }
    
    /**
     * Creates a random number of clients, find the best seat available in the Theater associated,
     * mark it available and print it out. If it realizes that the tickets are all sold out, it will
     * print out a sorry message.
     */
    public void run() {
	Random rand = new Random();
	int clients = rand.nextInt(900) + 100;
	for(int k = 0; k < clients; k +=1)
	{
	    Seat bestSeat = theater.bestSeatAvailable();
	    if(bestSeat != null)
	    {
		theater.markAvailableSeatTaken(bestSeat, id);
		theater.printTicketSeat(bestSeat);
		try {
		    Thread.sleep(100);
		} catch (InterruptedException e) {
		
		    e.printStackTrace();
		}
	    }
	    else
	    {
		System.out.println("Box Office " + id +": Sorry, we are sold out.");
		break;
	    }
	}
    }
}
