package assignment6;

import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Theater class contains all of the {@link Seat} objects which will represent seats within the theater. It associates them into
 * 4 priority queue: representing their location within the theater: front middle, front side, back middle, back side. It allows
 * the user to check with multiple threads which Seat is the best available. See more info for this in {@link Theater#bestSeatAvailable()}. 
 * After this, it allows the outside class to mark the seat unavailable, and then print it out. Will require that users 
 * do this in proper order or may not work properly.
 * @author Jayesh
 *
 */
public class Theater{
    private PriorityQueue<Seat> frontMiddle;
    private PriorityQueue<Seat> frontSide;
    private PriorityQueue<Seat> backMiddle;
    private PriorityQueue<Seat> backSide;
    private Lock theaterChangeLock;
    Theater()
    {
	initializeFrontMiddle();
	initializeFrontSide();
	intializeBackMiddle();
	initializeBackSide();
	theaterChangeLock = new ReentrantLock();
    }
    private void initializeBackSide() {
	backSide = new PriorityQueue<Seat>();
	for(char k = 'N'; k <= 'Z'; k +=1)
	{
	    if(k == 'O')
	    {
		continue;
	    }
	    for(int j = 101; j <= 128; j +=1)
	    {
		if(j == 108)
		{
		    j = 122;
		}
		Seat temp = new Seat(j, Character.toString(k));
		backSide.add(temp);
	    }
	}
	for(int j = 101; j <= 128; j +=1)
	{
	    if(j == 105)
	    {
		j = 116;
	    }
	    if(j == 119)
	    {
		j = 127;
	    }
	    Seat temp = new Seat(j, Character.toString((char) ('Z' + 1)));
	    backSide.add(temp);
	}
    }
    private void initializeFrontSide() {
	frontSide = new PriorityQueue<Seat>();
	for(char k = 'A'; k <= 'M'; k +=1)
	{
	    if(k == 'I')
	    {
		continue;
	    }
	    if(k != 'A' && k != 'B')
	    {
		for(int j = 101; j <= 107; j +=1)
		{
		    if( k == 'C' && j == 107)
		    {
			continue;
		    }
		    else
		    {
			Seat temp = new Seat(j, Character.toString(k));
			frontSide.add(temp);
		    }
		}
	    }
	    for(int j = 122; j <= 128; j +=1)
	    {
		Seat temp = new Seat(j, Character.toString(k));
		frontSide.add(temp);
	    }
	}
    }
    private void intializeBackMiddle() {
	backMiddle = new PriorityQueue<Seat>();
	for(char k = 'N'; k <= 'X'; k +=1)
	{
	    if(k == 'O')
	    {
		continue;
	    }
	    for(int j = 108; j < 121; j +=1)
	    {
		Seat temp = new Seat(j, Character.toString(k));
		backMiddle.add(temp);
	    }
	}
    }
    private void initializeFrontMiddle() {
	frontMiddle = new PriorityQueue<Seat>();
	for(char k = 'A'; k <= 'M'; k +=1)
	{
	    if(k == 'I')
	    {
		continue;
	    }
	    for(int j = 108; j < 121; j +=1)
	    {
		Seat temp = new Seat(j, Character.toString(k));
		frontMiddle.add(temp);
	    }
	}
    }
    
    /**
     * Returns the best {@link Seat} available in the Theater. It determines this by claiming that seats in the
     * Front Middle (A-M) (108-121) are the best seats followed by Front Sides (A-M) (101-107) (122 - 128), followed by
     * Back Middle (N-AA) (108-121) and finally by (N-AA) (101-107) (122-128). 
     * 
     * @return best Seat.
     */
    public Seat bestSeatAvailable()
    {
	theaterChangeLock.lock();
	Seat bestSeat;
    	try{
            	if(frontMiddle.peek() != null)
            	{
            	    bestSeat = frontMiddle.poll();
            	}
            	else if (frontSide.peek() != null)
            	{
            	    bestSeat = frontSide.poll();
            	}
            	else if (backMiddle.peek() != null)
            	{
            	    bestSeat = backMiddle.poll();
            	}
            	else if (backSide.peek() != null)
            	{
            	    bestSeat = backSide.poll();
            	}
            	else
            	{
            	    bestSeat = null;
            	}
    	}
	finally
	{
	    theaterChangeLock.unlock();
	}
	return bestSeat;
    }
    
    /**
     * Marks the Seat s available and taken by the BoxOffice boxOffice.
     * @param s - {@link Seat} which needs to be set available.
     * @param boxOffice - {@link BoxOffice} which has claimed the Seat
     */
    public void markAvailableSeatTaken(Seat s, char boxOffice)
    {
	s.setUnavailable(boxOffice);
    }
    
    /**
     * Prints out the ticket for the Seat which will use the {@link Seat#toString} method to get the String value that
     * represents each seat. 
     * 
     * @param s - Seat which needs to be printed. 
     */
    public void printTicketSeat(Seat s)
    {
	System.out.println(s.toString());
    }
}
