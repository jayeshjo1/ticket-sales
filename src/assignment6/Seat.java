package assignment6;

/**
 * The Seat class refers to each individual seat within the Theater. Each one will contain a number 
 * which will be set during the constructor phase, along with the name. Each will be initially set to
 * available in the beginning. When the outside class wants to set it unavailable, they must provide a 
 * character which represents the letter of the box office that has reserved the seat. This will be used 
 * later when the outside class wants to convert the seat into a string object which will return
 * a string containing the format {"Box Office (letter) ": Reserved HR, " (number) (name)}
 * @author Jayesh
 *
 */
public class Seat implements Comparable<Seat>{
    private int number;
    private String name;
    private char boxOffice;
    private boolean available;
    
    /**
     * When creating a Seat object, the constructor requires a name and number. The name will refer
     * to the character which represents the seat, and the number will be the location within the
     * name. 
     * 
     * @param num - number that represents the seat
     * @param nm - name that represe4nts the seat
     */
    Seat(int num, String nm)
    {
	name = nm;
	number = num;
	available = true;
	boxOffice = 0;
    }
    
    /**
     * @return returns the number associated with the Seat object that was assigned to it using the constructor.
     */
    public int getNumber()
    {
	return number;
    }
    
    /**
     * @return returns the name associated with the Seat object that was assigned to it using the constructor. 
     */
    public String getName()
    {
	return name;
    }
    
    /**
     * Sets the availability of the Seat to false. Requires a {@link BoxOffice} object which will designate the 
     * BoxOffice that claimed the seat.
     * 
     * @param boxOffice - the BoxOffice which claimed the seat
     */
    public void setUnavailable(char boxOffice)
    {
	this.boxOffice = boxOffice;
	available = false;
    }
    
    /**
     * @return Returns whether the Seat has been taken or not. If it has been taken, false, if it hasn't true.
     */
    public boolean isAvailable()
    {
	return available;
    }

    /**
     * Allows outside classes to compare two objects of Seat which will first compare the name and then the number.
     */
    public int compareTo(Seat o) {
	String current = name + number;
	String argument = o.getName() + o.getNumber();
	return current.compareTo(argument);
    }
    
    /**
     * Returns the String version of Seat. If the boxOffice has been instantiated, then will return a String containing
     * which which box office reserved the seat. Otherwise will return just the seat. The seat is represented by 
     * a strong containing first the number then the name of the string. 
     * 
     * @precondition Requires the user to have called {@link Seat#setUnavailable(char)} before calling it. Otherwise 
     * doesn't do a whole lot.
     */
    public String toString()
    {
	
	String combinedName;
	if(name.charAt(0) > 'Z')
	{
	    char first = (char) (((name.charAt(0) - 'A') / 26) + 'A' - 1);
	    char second = (char) (((name.charAt(0) - 'A') % 26) + 'A');
	    String combinedString = Character.toString(first) + Character.toString(second);
	    combinedName = number + combinedString;
	}
	else {
	    combinedName = number + name;
	}
	if(boxOffice == 0)
	{
	    return combinedName;
	}
	else {
	    return "Box Office " + boxOffice + ": Reserved HR, " + combinedName;
	}
    }
}
